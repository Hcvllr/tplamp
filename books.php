<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <title>Document</title>
</head>
<body>
    <?php 
        include('controller.php');

        $author='';
        $title='';
        $publisher='';
        $year='';
    ?>
<div class="container">

        <div class="form-group">
        <h1>Create Books</h1>
            <form method="post" action="books.php">
                <div class="col-sm">
                    <label for="Title"></label>
                    <input type="text" placeholder="Title" name="title">
                </div>
                <div class="col-sm">
                    <label for="Author"></label>
                    <input type="text" placeholder="Author" name="author">
                </div>
                <div class="col-sm">
                    <label for="Year"></label>
                    <input type="year" placeholder="Year" name="year">
                </div>
                <div class="col-sm"> 
                        <?php echo publisherList(); ?>
                </div>
                <div class="col-sm">
                    <button type="submit" name="Insert" values="0" class="btn btn-success">Insert</button>
                </div>
            </form>
        </div>


        <div class="form-group">
        <h1>Search Books</h1>
            <form method="post" action="books.php">
                <div class="col-sm">
                    <label for="Title"></label>
                    <input type="text" placeholder="Title" name="title">
                </div>
                <div class="col-sm">
                    <label for="Author"></label>
                    <input type="text" placeholder="Author" name="author">
                </div>
                <div class="col-sm">
                    <label for="Year"></label>
                    <input type="year" placeholder="Year" name="year">
                </div>
                <div class="col-sm"> 
                        <?php echo publisherList(); ?>
                </div>
                <div class="col-sm">
                    <button type="submit" name="Search" values="0" class="btn btn-info">Search</button>
                </div>
            </form>
        </div>

        <div class="container">
            <?php 

function contentTable($author, $title, $publisher, $year){
    
    $elements = searchBooks($author, $title, $publisher, $year);
    $tableau='';
    echo "<table class='table table-dark table-striped'>
    <thead>
        <tr><th>Author</th><th>Title</th><th>Publisher</th><th>Year</th></tr>
    </thead>
    <tbody>";
    foreach ($elements as $element) {
        $tableau .= '
        <tr>
        <form method="post" action="books.php?id=' . $element['id'] . '">
        <td>' . $element['author'] . '</td>
        <td>' . $element['title'] . '</td>
        <td>' . $element['publisher'] . '</td>
        <td>' . $element['year'] . '</td>
        <td><button type="submit" name="Delete" class="btn btn-danger">Delete</button></td>
        </form>
        </tr>';
    }
    echo $tableau;
}

if(isset($_POST['Insert'])){

    $title = $_POST["title"];
    $author = $_POST["author"];
    $publisher = $_POST["publisher_name"];
    $year = $_POST["year"];
    createBooks($title, $author, $publisher, $year);
}

elseif (isset($_POST['Delete'])) {

    if (isset($_GET['id']) && !empty($_GET['id']) && is_numeric($_GET['id'])) {

        $id = $_GET['id'];
        deleteBooks($id);
    }
}

elseif (isset($_POST['DeleteAll'])){
    $elements = searchBooks($author, $title, $publisher, $year);
    foreach ($elements as $element){
        $author = $element['author'];
    }
    DeleteAll($author);
}

elseif (isset($_POST['Search'])) {

    $title = $_POST["title"];
    $author = $_POST["author"];
    $publisher = $_POST["publisher_name"];
    $year = $_POST["year"];
    contentTable($author, $title, $publisher, $year);
    }
else{
    contentTable($author, $title, $publisher, $year);
}



function publisherList()
{
    $pdo = connect();
    $sqlrequest = "SELECT publisher_name FROM publisher";
    $rst = $pdo->query($sqlrequest);
    $elements = $rst->fetchAll();
    echo "<select name='publisher_name'>
    <option></option>;";
    foreach ($elements as $element) {
        $list .= '
        <option>' . $element['publisher_name'] . '</option>';
    }
    echo $list;
    echo "</select>";
}

?>
        </div>
</body>
</html>