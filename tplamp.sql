-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 21 fév. 2020 à 22:04
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `tplamp`
--
CREATE DATABASE IF NOT EXISTS `tplamp` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `tplamp`;

-- --------------------------------------------------------

--
-- Structure de la table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `author` text NOT NULL,
  `title` text NOT NULL,
  `year` int(11) NOT NULL,
  `publisher` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `books`
--

INSERT INTO `books` (`id`, `author`, `title`, `year`, `publisher`) VALUES
(1, 'JK Rowling', 'Harry Potter à l école des sorciers', 1997, 'Bloomsburry Publishing'),
(2, 'JK Rowling', 'Harry Potter et la Chambre des Secrets', 1998, 'Bloomsburry Publishing'),
(3, 'Dicolas Senpaï', 'Ma vie, mon œuvre ', 2020, 'Gallimard'),
(6, 'Alexandre Dumas Jr', 'L étroit mousquetaire', 2019, 'Hachette'),
(7, 'Jean-Marie Bigard', 'Le gros Abécédaire de Jean-Marie Bigard', 2017, 'Hachette'),
(8, 'Jean-Marie Bigard', '1 kilo de blagues', 2015, 'Hachette'),
(9, 'Maïté', 'La cuisine bonne', 1995, 'Hachette'),
(11, 'Team21', 'La fabuleuse histoire des AP21', 2020, 'Flammarion'),
(12, 'Arouf Blanco', 'Le plus Hlou des réunionnais', 2019, 'Flammarion'),
(14, 'Dicolas Brazeub', 'L Histoire de Dico', 2020, 'Hachette'),
(18, 'Toninou', 'Les mésaventures du train', 2020, 'Hachette');

-- --------------------------------------------------------

--
-- Structure de la table `publisher`
--

CREATE TABLE `publisher` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `publisher_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `publisher`
--

INSERT INTO `publisher` (`id`, `publisher_name`) VALUES
(1, 'Bloomsburry Publishing'),
(2, 'Hachette'),
(3, 'Gallimard'),
(4, 'Flammarion');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'hugo', 'toto');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `publisher`
--
ALTER TABLE `publisher`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `publisher`
--
ALTER TABLE `publisher`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
